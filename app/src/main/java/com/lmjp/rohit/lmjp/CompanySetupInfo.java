package com.lmjp.rohit.lmjp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CompanySetupInfo extends AppCompatActivity {

    EditText regId,comName,comAddress,nofEmp,comDesc;
    Spinner sector,comType;
    Button getStarted;
    FirebaseAuth mAuth;
    String Comsector,Type;
    DatabaseReference mref;
    UserManagement userManagement;
    FirebaseUser user;
    String[] Sector = new String[]{"Select Company Sector","5Start MIDC Kagal","Gokul Shirgaon MIDC","Shiroli MIDC"};
    String[] ComType = new String[]{"Select Company Type","Dairy","Laundry","Manufacturing","Faundry","CNC Industries","Plumbing","Textile Industries","Oil Industries","Petrol Pump","Servicing"};
    ArrayAdapter<String> sectorAdapter,comTypeAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_setup_info);
        mAuth = FirebaseAuth.getInstance();
        mref = FirebaseDatabase.getInstance().getReference("MIDC");
        user = mAuth.getCurrentUser();
        regId = findViewById(R.id.editTextCompanyReg);
        comName = findViewById(R.id.editTextComName);
        comAddress = findViewById(R.id.editTextComAddress);
        nofEmp = findViewById(R.id.editTextnoEmp);
        comDesc = findViewById(R.id.editTextComDesc);
        sector = findViewById(R.id.companySector);
        comType = findViewById(R.id.comType);
        sectorAdapter = new ArrayAdapter<String>(CompanySetupInfo.this,android.R.layout.simple_spinner_item,Sector);
        comTypeAdapter = new ArrayAdapter<String>(CompanySetupInfo.this,android.R.layout.simple_spinner_item,ComType);
        sectorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        comTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sector.setAdapter(sectorAdapter);
        comType.setAdapter(comTypeAdapter);
        sector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Comsector = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        comType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Type = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        getStarted = findViewById(R.id.buttonGetStarted);
        userManagement = new UserManagement(getApplicationContext());
        getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userManagement.saveCompanyDetails(Comsector,regId.getText().toString(),comName.getText().toString(),comAddress.getText().toString(),Type,nofEmp.getText().toString(),comDesc.getText().toString());
                if(Comsector.equals("5Start MIDC Kagal")){

                    DatabaseReference comRef= mref.child(Comsector).child(regId.getText().toString());
                    comRef.child("RegistrationID").setValue(regId.getText().toString());
                    comRef.child("Sector").setValue(Comsector);
                    comRef.child("CompanyName").setValue(comName.getText().toString());
                    comRef.child("ComAddress").setValue(comAddress.getText().toString());
                    comRef.child("ComType").setValue(Type);
                    comRef.child("NoOfEmployee").setValue(nofEmp.getText().toString());
                    comRef.child("CompanyDesc").setValue(comDesc.getText().toString());
                    Toast.makeText(getApplicationContext(),"Added Successfully",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(),CompanyOwnerActivity.class));

                }

                else if(Comsector.equals("Gokul Shirgaon MIDC")){


                    DatabaseReference comRef= mref.child(Comsector).child(regId.getText().toString());
                    comRef.child("RegistrationID").setValue(regId.getText().toString());
                    comRef.child("Sector").setValue(Comsector);
                    comRef.child("CompanyName").setValue(comName.getText().toString());
                    comRef.child("ComAddress").setValue(comAddress.getText().toString());
                    comRef.child("ComType").setValue(Type);
                    comRef.child("NoOfEmployee").setValue(nofEmp.getText().toString());
                    comRef.child("CompanyDesc").setValue(comDesc.getText().toString());

                    Toast.makeText(getApplicationContext(),"Added Successfully",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(),CompanyOwnerActivity.class));

                }

                else if(Comsector.equals("Shiroli MIDC")){


                    DatabaseReference comRef= mref.child(Comsector).child(regId.getText().toString());
                    comRef.child("RegistrationID").setValue(regId.getText().toString());
                    comRef.child("Sector").setValue(Comsector);
                    comRef.child("CompanyName").setValue(comName.getText().toString());
                    comRef.child("ComAddress").setValue(comAddress.getText().toString());
                    comRef.child("ComType").setValue(Type);
                    comRef.child("NoOfEmployee").setValue(nofEmp.getText().toString());
                    comRef.child("CompanyDesc").setValue(comDesc.getText().toString());
                    Toast.makeText(getApplicationContext(),"Added Successfully",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(),CompanyOwnerActivity.class));

                }
                else{

                }
            }
        });
    }

    @Override
    public void onStart(){

        super.onStart();
        if(userManagement.isFiled()){
            startActivity(new Intent(getApplicationContext(),CompanyOwnerActivity.class));
        }
    }

}
