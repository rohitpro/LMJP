package com.lmjp.rohit.lmjp;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


/**
 * A simple {@link Fragment} subclass.
 */
public class ManagementCommiteeAccounts extends Fragment {

    private static final CharSequence ALPHA = "123ABCDEFGHIJ456VWXYZ789KLMNOPQRSTU";

    EditText email;
    Button create;
    FirebaseAuth mAuth;
    public ManagementCommiteeAccounts() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_management_commitee_accounts, container, false);
        email = view.findViewById(R.id.editTextEmail);
        mAuth = FirebaseAuth.getInstance();
        create = view.findViewById(R.id.buttonCreate);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final String Email = email.getText().toString();
                final String Pass = randomPassword(8);
                final String Message = "Your account Details For LMJP System.\n" +
                        "Email ID : " + " "+Email+"\n"+
                        "Password:  " + " "+Pass+ "\n"+ " ";

                mAuth.createUserWithEmailAndPassword(Email,Pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful()){
                        SendEmail send = new SendEmail(getContext(),Email,"Account Creation Details",Message);
                        send.execute();
                            Toast.makeText(getContext(),"Account created",Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Snackbar.make(v,"Something went wrong",Snackbar.LENGTH_LONG).show();
                        }
                    }
                });

            }
        });

       return view;
    }


    public static String randomPassword(int count)
    {
        StringBuilder builder = new StringBuilder();
        while(count-- !=0)
        {
            int character = (int)(Math.random()*ALPHA.length());
            builder.append(ALPHA.charAt(character));
        }
        return builder.toString();
    }

}
