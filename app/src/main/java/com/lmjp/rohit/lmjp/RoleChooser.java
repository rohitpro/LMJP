package com.lmjp.rohit.lmjp;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

import com.google.android.gms.auth.api.Auth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RoleChooser extends AppCompatActivity {

    private View roleView;
    RadioButton jobSeeker,comManager;
    Intent intent;
    DatabaseReference mref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_role_chooser);
        roleView = getLayoutInflater().inflate(R.layout.role_chooser,null);
        jobSeeker = roleView.findViewById(R.id.jobSeeker);
        comManager = roleView.findViewById(R.id.comManager);
        mref = FirebaseDatabase.getInstance().getReference("CompanyInfo");
        jobSeeker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                  intent = new Intent(getApplicationContext(),Authentication.class);
                    startActivity(intent);
            }
        });

        comManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 intent = new Intent(getApplicationContext(),CompanyAuthentication.class);
                 startActivity(intent);
            }
        });
        roleChooser();
    }


    public void roleChooser() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog = builder.create();
        dialog.setView(roleView);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

}
