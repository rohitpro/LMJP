package com.lmjp.rohit.lmjp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.concurrent.TimeUnit;

public class Authentication extends AppCompatActivity {

    private View view;
    Intent intent;
    View roleView;
    String code;
    private EditText mobno,verificationCode;
    Button btnSignIn,btnVerifyCode;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    private PhoneAuthProvider.ForceResendingToken mresendToken;
    private String mobNo;
    private FirebaseAuth mAuth;
    String JobSeekerType,ComManager;
    FirebaseUser user;
    DatabaseReference reference,mref;
     @Override
    protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);

         setContentView(R.layout.activity_authentication);
         view = getLayoutInflater().inflate(R.layout.verifycode, null);
         mobno = (EditText) findViewById(R.id.mobno);
         btnSignIn = (Button) findViewById(R.id.btnSignIn);
         btnVerifyCode = (Button) view.findViewById(R.id.btnVerifyCode);
         verificationCode = (EditText) view.findViewById(R.id.verificationCode);
         mAuth = FirebaseAuth.getInstance();
         reference = FirebaseDatabase.getInstance().getReference("UserProfile");

         mref = FirebaseDatabase.getInstance().getReference("CompanyInfo");
//        mref.push().child("companyId").setValue("902341541");
         btnVerifyCode.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 String Code = verificationCode.getText().toString();
                 verifyCode(code, Code);
             }
         });


         callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
             @Override
             public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                 String code = phoneAuthCredential.getSmsCode();
                 getAutomaticCodeRetrieval(code);
             }

             @Override
             public void onVerificationFailed(FirebaseException e) {

                 Toast.makeText(getApplicationContext(), "Something went wrong" + e.toString(), Toast.LENGTH_LONG).show();
             }

             @Override
             public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                 super.onCodeSent(s, forceResendingToken);
                 Toast.makeText(getApplicationContext(), "Verification code has been sent", Toast.LENGTH_LONG).show();
                 code = s;
                 mresendToken = forceResendingToken;
                 //verificationCode.setText(mresendToken.toString());
             }

             @Override
             public void onCodeAutoRetrievalTimeOut(String s) {
                 super.onCodeAutoRetrievalTimeOut(s);
                 //Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
             }

         };
     }
    @Override
    public void onStart(){
        super.onStart();
            user = mAuth.getCurrentUser();
            if (user != null) {
                startActivity(new Intent(getApplicationContext(), Home.class));
            }
    }


    public void verifyUser(View v){

        mobNo = mobno.getText().toString().trim();
        ViewGroup parent = (ViewGroup) view.getParent();
        if(mobNo.isEmpty())
        {
            mobno.setError("Required field");
        }
        else if(mobNo.length()<10 || mobNo.length()>10 )
        {
            mobno.setError("Enter 10 digit only");
        }
        else {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    "+91"+mobNo,
                    60,
                    TimeUnit.SECONDS,
                    Authentication.this,
                    callbacks);
                  if(parent ==null) {
                openVerificationWindow();
            }
            else{
                parent.removeView(view);
                openVerificationWindow();
            }
        }


    }


    public void openVerificationWindow()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(Authentication.this);
        AlertDialog dialog = builder.create();
        dialog.setView(view);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void getAutomaticCodeRetrieval(String Code)
    {
        verificationCode.setText(Code);
        verifyCode(code,Code);
    }

    public void verifyCode(String verificationCode,String code)
    {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCode,code);
        signInWithPhoneAuthCredential(credential);
        //showDialog();
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {

        user=mAuth.getCurrentUser();
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful())
                {

                    startActivity(new Intent(getApplicationContext(),Home.class));
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_LONG).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            }
        });
    }


}
