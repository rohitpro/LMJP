package com.lmjp.rohit.lmjp;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * A simple {@link Fragment} subclass.
 */
public class Profile extends Fragment {

    private static final int SELECT_FILE = 1;
    private static final int REQUEST_CAMERA = 0;
    FirebaseAuth mAuth;
    DatabaseReference mref;
    FirebaseUser user;
    View view;
    TextView profile;
    CircleImageView updateProfile,uploadPhoto,profileImage;
    private String userChoosenTask;
    private Uri mImageUri = null;
    View profileView;

    Button btn_profileUpdate;
    EditText fname,address,emailid;
    StorageReference sRef;
    TextView Fullname,Address,EmailID;
    private AlertDialog dialog;

    public Profile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        profileView = inflater.inflate(R.layout.update_profile,null);
        mAuth = FirebaseAuth.getInstance();
        mref = FirebaseDatabase.getInstance().getReference("UserProfile");
        user = mAuth.getCurrentUser();
        sRef = FirebaseStorage.getInstance().getReference();
        profile = view.findViewById(R.id.profile);
        updateProfile = view.findViewById(R.id.updateProfile);
        uploadPhoto = view.findViewById(R.id.updatePhoto);
        profileImage = view.findViewById(R.id.profileImage);

        Fullname = view.findViewById(R.id.TextView_fullname);
        Address = view.findViewById(R.id.TextView_address);
        EmailID = view.findViewById(R.id.TextView_emailId);

        profile.setText("MobNo:"+ " "+user.getPhoneNumber());
        btn_profileUpdate = profileView.findViewById(R.id.btn_updateInfo);
        fname = profileView.findViewById(R.id.fname);
        address = profileView.findViewById(R.id.address);
        emailid = profileView.findViewById(R.id.emailid);
        btn_profileUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mref.child(user.getUid()).child("fullName").setValue(fname.getText().toString());

                mref.child(user.getUid()).child("address").setValue(address.getText().toString());

                mref.child(user.getUid()).child("emailId").setValue(emailid.getText().toString());
                Toast.makeText(getContext(),"Profile has been updated",Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        getProfile();
        uploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
                    }
        });

        updateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewGroup parent = (ViewGroup) profileView.getParent();
                if(parent == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    dialog = builder.create();
                    dialog.setView(profileView);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else{
                    parent.removeView(profileView);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    dialog = builder.create();
                    dialog.setView(profileView);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
            }
        });
        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermissionUtility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }





    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= PermissionUtility.checkPermission(getContext());

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                                    }
            }
        });
        builder.show();
    }

    private void galleryIntent() {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);

    }


    public void cameraIntent(){


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mImageUri = data.getData();

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        profileImage.setImageBitmap(thumbnail);
        uploadToFirebase();
    }

    private void onSelectFromGalleryResult(Intent data) {

        try {
            Bitmap bm;
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), data.getData());
                    profileImage.setImageBitmap(bm);
                    uploadToFirebase();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        }
        catch (Exception ex){

            Toast.makeText(getContext(),ex.toString(),Toast.LENGTH_SHORT).show();
        }
    }

    public void uploadToFirebase(){

        try {
            StorageReference uploadRef = sRef.child("UserImages").child(user.getUid()).child(mImageUri.getLastPathSegment());
            uploadRef.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    String imageUrl = taskSnapshot.getDownloadUrl().toString();
                    mref.child(user.getUid()).child("photoUrl").setValue(imageUrl);
                    Toast.makeText(getContext(), "Uploaded successfully", Toast.LENGTH_SHORT).show();
                    getProfile();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception ex){
            Toast.makeText(getContext(),ex.toString(),Toast.LENGTH_SHORT).show();
        }
    }

    private void getProfile() {
        try {

            mref.child(user.getUid()).child("photoUrl").addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    boolean result = dataSnapshot.exists();
                    //Toast.makeText(getContext(),"Status"+result,Toast.LENGTH_SHORT).show();
                    if(result){

                        String url =  dataSnapshot.getValue().toString();
                        Picasso.with(getContext()).load(Uri.parse(url)).fit().into(profileImage);

                    }else {

                        String Url= "https://firebasestorage.googleapis.com/v0/b/lmjp-67c25.appspot.com/o/defaultImage%2Fuser.png?alt=media&token=7b9bc15a-e854-43b1-9729-36f4c3c0e1bc";

                        Picasso.with(getContext()).load(Uri.parse(Url)).fit().into(profileImage);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            mref.child(user.getUid()).child("fullName").addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    boolean result = dataSnapshot.exists();
                    //Toast.makeText(getContext(),"Status"+result,Toast.LENGTH_SHORT).show();
                    if(result){

                        String fullName =  dataSnapshot.getValue().toString();
                        Fullname.setText("Full Name:"+ " "+ fullName);

                    }else {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });



            mref.child(user.getUid()).child("address").addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    boolean result = dataSnapshot.exists();
                    //Toast.makeText(getContext(),"Status"+result,Toast.LENGTH_SHORT).show();
                    if(result){

                        String fullName =  dataSnapshot.getValue().toString();
                        Address.setText("Address:"+ " "+ fullName);

                    }else {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            mref.child(user.getUid()).child("emailId").addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    boolean result = dataSnapshot.exists();
                    //Toast.makeText(getContext(),"Status"+result,Toast.LENGTH_SHORT).show();
                    if(result){

                        String fullName =  dataSnapshot.getValue().toString();
                        EmailID.setText("Email ID:"+ " "+ fullName);

                    }else {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


        }
        catch (Exception ex){

            Toast.makeText(getContext(),ex.toString(),Toast.LENGTH_SHORT).show();
        }

    }
}
