package com.lmjp.rohit.lmjp;

/**
 * Created by gdevop on 16/4/18.
 */

public class JobList {

    String description,jobTitle,ageLimit,vacancy,qualification;
    public JobList() {

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getAgeLimit() {
        return ageLimit;
    }

    public void setAgeLimit(String ageLimit) {
        this.ageLimit = ageLimit;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getVacancy() {
        return vacancy;
    }

    public void setVacancy(String vacancy) {
        this.vacancy = vacancy;
    }

    public JobList(String description, String jobTitle, String ageLimit, String vacancy,String qualification) {

        this.description = description;
        this.jobTitle = jobTitle;
        this.ageLimit = ageLimit;
        this.vacancy = vacancy;
        this.qualification = qualification;
    }
}
