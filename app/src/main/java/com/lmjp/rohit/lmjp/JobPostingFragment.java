package com.lmjp.rohit.lmjp;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * A simple {@link Fragment} subclass.
 */
public class JobPostingFragment extends Fragment {

    EditText editJobtitle,editJobDesc,editAge,editVacancy,editQualification;
    Button post;
    Spinner qualification;
    String[] arr = new String[]{"Select Qualification","ITI pass","10th pass","12th pass","B.SC/B.COM/B.A","Other"};
    ArrayAdapter <String> adapter;
    View qualificationView;
    String Qual;
    UserManagement userManagement;
    DatabaseReference drefer;
    FirebaseAuth mauth;
    FirebaseUser muser;

    public JobPostingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_job_posting, container, false);
        qualificationView = inflater.inflate(R.layout.qualification_dialog,null);
        editJobtitle=view.findViewById(R.id.edittextJobTitle);
        editJobDesc=view.findViewById(R.id.editTextJobDescription);
        editAge =view.findViewById(R.id.editTextAgeLimit);
        editVacancy=view.findViewById(R.id.editTextVacancy);
        post=view.findViewById(R.id.btnPost);
        editQualification = qualificationView.findViewById(R.id.editTextQualification);
        qualification=view.findViewById(R.id.spinner2);
        adapter=new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,arr);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        qualification.setAdapter(adapter);
        mauth = FirebaseAuth.getInstance();
        muser = mauth.getCurrentUser();
        userManagement = new UserManagement(getContext());
        drefer = FirebaseDatabase.getInstance().getReference("MIDC");
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title,desc,vacancy,agelimit;
                title = editJobtitle.getText().toString();
                desc = editJobDesc.getText().toString();
                vacancy = editVacancy.getText().toString();
                agelimit = editAge.getText().toString();
                postnewJob(title,desc,vacancy,agelimit,Qual);
            }
        });
        qualification.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(!(parent.getItemAtPosition(position).toString().equals("Other"))){

                    Qual = parent.getItemAtPosition(position).toString();
                }

                if(parent.getItemAtPosition(position).toString().equals("Other")){

                    ViewGroup p = (ViewGroup) qualificationView.getParent();
                    if(p==null) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.setView(qualificationView);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                        Qual = editQualification.getText().toString();
                    }
                    else{

                        p.removeView(qualificationView);
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.setView(qualificationView);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                        Qual = editQualification.getText().toString();
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

    public void postnewJob(String title,String descp,String vacancy,String agelimit,String qualiFication)
    {
        DatabaseReference dref = drefer.child(userManagement.getSector()).child(userManagement.getRegID()).child("PostedJob").child(muser.getUid()).push();
        dref.child("jobTitle").setValue(title);
        dref.child("description").setValue(descp);
        dref.child("vacancy").setValue(vacancy);
        dref.child("ageLimit").setValue(agelimit);
        dref.child("qualification").setValue(qualiFication);
        Toast.makeText(getContext(),"Job posted",Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getContext(),CompanyOwnerActivity.class));
    }
}
