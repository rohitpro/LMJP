package com.lmjp.rohit.lmjp;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by gdevop on 15/4/18.
 */

public class UserManagement {
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "Aaryaa";

    // All Shared Preferences Keys
    private static final String IS_FILLED = "IsLoggedIn";

    private static final String IS_Com_FILLED = "IsFilled";


    // User name (make variable public to access from outside)
    public static final String KEY_sector = "sector";

    public static final String KEY_regID = "regID";

    public static final String KEY_comName = "comName";

    public static final String KEY_comAddr = "comAddr";

    public static final String KEY_ComType = "comType";

    public static final String KEY_NoEmp = "NoEmp";

    public static final String KEY_CompDesc = "ComDesc";

    public static final String KEY_NAME = "type";


    public UserManagement(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createStaffSession(String type) {
        // Storing login value as TRUE
        editor.putBoolean(IS_FILLED, true);
        // Storing name in pref
        editor.putString(KEY_NAME, type);
        editor.commit();
    }

    public void saveCompanyDetails(String sector,String regId,String comName,String comAdd,String comType,String noEmp,String comDesc) {
        // Storing login value as TRUE
        editor.putBoolean(IS_Com_FILLED, true);
        // Storing name in pref
        editor.putString(KEY_sector, sector);

        editor.putString(KEY_regID, regId);

        editor.putString(KEY_comName, comName);

        editor.putString(KEY_comAddr, comAdd);

        editor.putString(KEY_ComType, comType);

        editor.putString(KEY_NoEmp, noEmp);

        editor.putString(KEY_CompDesc, comDesc);
        editor.commit();
    }

    public String getType(){

        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString(KEY_NAME,"");
    }

    public String getSector(){

        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString(KEY_sector,"");
    }
    public String getRegID(){

        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString(KEY_regID,"");
    }
    public String getComName(){

        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString(KEY_comName,"");
    }
    public String getComAddr(){

        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString(KEY_comAddr,"");
    }
    public String getComType(){

        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString(KEY_ComType,"");
    }
    public String getNoEmp(){

        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString(KEY_NoEmp,"");
    }
    public String getComDesc(){

        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString(KEY_CompDesc,"");
    }


    public boolean isFiled(){

        return pref.getBoolean(IS_Com_FILLED, false);
    }

}
