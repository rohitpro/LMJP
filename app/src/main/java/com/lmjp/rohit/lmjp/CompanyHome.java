package com.lmjp.rohit.lmjp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * A simple {@link Fragment} subclass.
 */
public class CompanyHome extends Fragment {

    RecyclerView recyclerView;
    DatabaseReference dref;
    FirebaseAuth mAuth;
    FirebaseUser user;
    private View view, jobView;
    UserManagement userManagement;

    public CompanyHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_company_home, container, false);
//        jobView = inflater.inflate(R.layout.job_list_view,null);
        mAuth = FirebaseAuth.getInstance();
        dref = FirebaseDatabase.getInstance().getReference("MIDC");
        user = mAuth.getCurrentUser();
        recyclerView = view.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        userManagement = new UserManagement(getContext());

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            DatabaseReference jobRef = dref.child(userManagement.getSector()).child(userManagement.getRegID()).child("PostedJob").child(user.getUid());

            Toast.makeText(getContext(), "Ref" + jobRef, Toast.LENGTH_SHORT).show();
            FirebaseRecyclerAdapter<JobList, JobHolder> recyclerAdapter = new FirebaseRecyclerAdapter<JobList, JobHolder>(JobList.class, R.layout.job_list_view, JobHolder.class, jobRef) {
                @Override
                protected void populateViewHolder(JobHolder jobHolder, JobList jobList, int i) {

                    jobHolder.setJobTitle(jobList.getJobTitle());
                    jobHolder.setJobDesc(jobList.getDescription());
                    jobHolder.setJobVacancy(jobList.getVacancy());
                    jobHolder.setJobAge(jobList.getAgeLimit());
                    jobHolder.setJobQual(jobList.getQualification());
                }
            };
            recyclerView.setAdapter(recyclerAdapter);
        } catch (Exception ex) {

            Toast.makeText(getContext(), ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    public static class JobHolder extends RecyclerView.ViewHolder {

        View jobView;

        public JobHolder(View itemView) {
            super(itemView);
            jobView = itemView;
        }

        public void setJobTitle(String JobTitle) {
            TextView jobtitle = jobView.findViewById(R.id.textViewJobTitle);
            jobtitle.setText("Job Title:"+" "+JobTitle);
        }

        public void setJobDesc(String description) {
            TextView desc = jobView.findViewById(R.id.textViewDesc);
            desc.setText("Job Desription:"+ " "+description);
        }

        public void setJobVacancy(String Vacancy) {
            TextView vacancy = jobView.findViewById(R.id.textViewvac);
            vacancy.setText("Vacancy:"+" "+Vacancy);
        }

        public void setJobAge(String ageLimit) {
            TextView age = jobView.findViewById(R.id.textViewage);
            age.setText("Age Limit:"+ " "+ageLimit);
        }


        public void setJobQual(String qualification) {
            TextView qual = jobView.findViewById(R.id.textViewqual);
            qual.setText("Job Qualification:"+ " "+qualification);

        }

    }
}