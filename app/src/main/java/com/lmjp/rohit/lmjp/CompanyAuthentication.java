package com.lmjp.rohit.lmjp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class CompanyAuthentication extends AppCompatActivity {

    AutoCompleteTextView email, password;
    Button signIn, signUp;
    String Role[] = new String[]{"Select Your Role", "Company Owner", "Worker Team"};
    Spinner role;
    ArrayAdapter<String> roleType;
    FirebaseAuth mAuth;
    DatabaseReference mref,accountRef;
    FirebaseUser user;
    EditText editEmail,editPass,editComID;
    Button register;
    View registerView;
    private String id;
    private String type;
    private String RoleType;
    UserManagement userManagement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_authentication);
        registerView = getLayoutInflater().inflate(R.layout.owner_registration,null);
        email = findViewById(R.id.autoCompleteTextViewEmail);
        password = findViewById(R.id.autoCompleteTextViewPassword);
        role = findViewById(R.id.spinner);
        mAuth = FirebaseAuth.getInstance();
        mref = FirebaseDatabase.getInstance().getReference("CompanyInfo");
        signIn = findViewById(R.id.button2);
        signUp = findViewById(R.id.button3);

        userManagement = new UserManagement(getApplicationContext());
        editEmail = registerView.findViewById(R.id.editTextEmail);
        editPass = registerView.findViewById(R.id.editTextPass);
        editComID = registerView.findViewById(R.id.editTextComID);

        register = registerView.findViewById(R.id.buttonRegister);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = editEmail.getText().toString();
                String pass = editPass.getText().toString();
                String comID = editComID.getText().toString();
                showCompanyId(email,pass,comID);

            }
        });

        roleType = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Role);
        roleType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        role.setAdapter(roleType);
        role.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                 type = parent.getItemAtPosition(position).toString();
                if (type.equals("Company Owner")) {

                    Toast.makeText(getApplicationContext(), "Comming Soon" + type, Toast.LENGTH_SHORT).show();
                    signUp.setClickable(true);
                    signUp.setEnabled(true);
                    signUp.setVisibility(View.VISIBLE);
                    AlertDialog.Builder builder = new AlertDialog.Builder(CompanyAuthentication.this);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.setTitle("Warning!");
                    dialog.setMessage("You are selected role as Comapny Owner. Don't share your Company ID with anyone else. If you don't have an account please click on 'SIGN UP' button. Thank You!!!");
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();

                } else if (type.equals("Worker Team")) {

                    Toast.makeText(getApplicationContext(), "Comming Soon" + type, Toast.LENGTH_SHORT).show();

                    signUp.setClickable(false);
                    signUp.setEnabled(false);
                    signUp.setVisibility(View.INVISIBLE);
                    AlertDialog.Builder builder = new AlertDialog.Builder(CompanyAuthentication.this);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.setTitle("LMJP");
                    dialog.setMessage("If you have an account credentials then you can directly login into your account. Else Please check your account credentials on Your gmail account. If not Please contact to Company Owner to generate account credentials. Thank You!!!");
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();


                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mref = FirebaseDatabase.getInstance().getReference("CompanyInfo");
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if(type.equals("Company Owner")){

                    RoleType = "Company Owner";
                    userManagement.createStaffSession(RoleType);
                    String Email = email.getText().toString();
                    String Pass = password.getText().toString();
                    mAuth.signInWithEmailAndPassword(Email,Pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if(task.isSuccessful()){

                                Intent intent = new Intent(getApplicationContext(),CompanySetupInfo.class);
                                intent.putExtra("type",RoleType);
                                startActivity(intent);
                                finish();
                            }
                            else {

                                Snackbar.make(v,"Authentication failed",Snackbar.LENGTH_LONG).show();
                            }
                        }
                    });

                }
                else if(type.equals("Worker Team")){

                    RoleType = "Worker Team";
                    userManagement.createStaffSession(RoleType);
                    String Email = email.getText().toString();
                    String Pass = password.getText().toString();

                    mAuth.signInWithEmailAndPassword(Email,Pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if(task.isSuccessful()){

                                Intent intent = new Intent(getApplicationContext(),CompanyOwnerActivity.class);
                                intent.putExtra("type",RoleType);
                                startActivity(intent);
                                finish();
                            }
                            else {

                                Snackbar.make(v,"Authentication failed",Snackbar.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewGroup parent = (ViewGroup) registerView.getParent();
                if(parent ==null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(CompanyAuthentication.this);
                    AlertDialog dialog = builder.create();
                    dialog.setView(registerView);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else{
                    parent.removeView(registerView);
                    AlertDialog.Builder builder = new AlertDialog.Builder(CompanyAuthentication.this);
                    AlertDialog dialog = builder.create();
                    dialog.setView(registerView);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
            }
        });


    }

    @Override
    public void onStart(){

        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if(user!=null){

            startActivity(new Intent(CompanyAuthentication.this,CompanySetupInfo.class));
        }
    }

    public void showCompanyId(final String email, final String pass, final String comID){

        mref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot companyID:dataSnapshot.getChildren()){

                    id = companyID.child("CompanyId").getValue().toString();
                    if(id.equals(comID)){
                     mAuth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                         @Override
                         public void onComplete(@NonNull Task<AuthResult> task) {

                             if(task.isSuccessful()){

                                 startActivity(new Intent(getApplicationContext(),CompanySetupInfo.class));
                                 finish();
                             }
                             else{

                                 Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                             }
                         }
                     });
                    }
                }
                if(!(id.equals(comID))){

                    editComID.setError("Invalid ID");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}